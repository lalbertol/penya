<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\Mail;


class BasketController extends Controller
{
        public function __construct()
  {
      $this->middleware('auth'); //aplicable a todos los métodos
      //$this->middleware('log')->only('index');//solamente a ....
      //$this->middleware('subscribed')->except('store');//todos excepto ...
  }
    public function index(Request $request)
    {
        //$products = Product::paginate(10);
        $total = 0;

         $basket = $request->session()->get('basket');
        if (! $basket ) {$basket = [];}
            return view('basket.index', ['products' => $basket],['total' => $total]);
        //return $products;

    }

 public function addSession(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $basket = $request->session()->get('basket');
        //buscar usuarios


         if ($basket == null) {
             $basket = array();
         }

         $position = -1;
         foreach ($basket as $key => $item) {
             if ($item->id == $id) {
                $item->cantidad++;
                $position = $key;


                break;
             }
         }

         if ($position == -1) {

            $product->cantidad = 1;
             $request->session()->push('basket', $product);
         }
         return redirect('/basket');

    }

    public function flush(Request $request)
    {

       $request->session()->forget('basket');


        return back();
    }

    public function destroy(Request $request,$id)
    {
        //echo "hola";
         $basket = Session::get('basket');

           foreach ($basket as $key => $item) {

             if ($item->id == $id) {
                //$request->session()->forget('basket'.$item);
                $request->session()->forget('basket.'.$key);
                //unset($basket[$item]);
                return back();
                //break;
             }
         }

          //$request->session()->forget('basket', $product);


        //return redirect('/basket');
    }
     public function add(Request $request,$id)
    {
        $basket = Session::get('basket');
       $product = Product::findOrFail($id);

       foreach ($basket as $key => $item) {

             if ($item->id == $id) {

                $item->cantidad++;

                return back();
             }
         }



    }
       public function reduce(Request $request,$id)
    {
        $basket = Session::get('basket');

        $product = Product::findOrFail($id);

        foreach ($basket as $key => $item) {

              if ($item->id == $id) {
                 if ($item->cantidad == 1) {
                     $request->session()->forget('basket.'.$key);

                 }else{
                     $item->cantidad--;
                }



                return redirect('/basket');
             }
          }



    }

    public function store(Request $request)
    {
        $basket = Session::get('basket');
        $date = Carbon::today();
        $user = \Auth::user();

       // dd($user);
        $order = new Order();
        $order->user_id = $user->id;
        $order->paid = 0;
        $order->date = $date;

        $order->save();


        foreach ($basket as $product) {
            $order->products()->attach($product->id,
        [
            'order_id' => $order->id,
            'price' => $product->price,
            'quantity' => $product->cantidad,
            ]);

        }

        $request->session()->forget('basket');


  $email = $user->email;

              Mail::send('mail.mail', [], function($message) use ($email)
{
    $message->to($email)->subject('Email del producto')->attachData($pdf->output(),'Pedido.pdf');
});



        return back();


    }


}
