<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;

class CathegoriesController extends Controller
{
       public function __construct()
  {
      //$this->middleware('auth'); //aplicable a todos los métodos
      //$this->middleware('log')->only('index');//solamente a ....
      $this->middleware('auth')->except('index','show');//todos excepto ...
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();

         $cathegories = Cathegory::paginate(10);
        //$cathegories = Cathegorie::all();
        //return $cathegories;
        return view('cathegories.index', ['cathegories' => $cathegories],['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('cathegories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $rules = [
            'name' => 'required|max:255|min:3',

            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);


        // $user = new User();
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));

        // $user->remember_token = str_random(10);
        // $user->save();

        // //opcion2
        // $user = User::create($request->all());

        //opcion3
        $cathegory = new Cathegory();
        $cathegory->fill($request->all());

        $cathegory->save();

        return redirect('/cathegories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cathegory $cathegory)
    {
        //$cathegorie = Cathegorie::with('products')->findOrFail($id);
        //$cathegorie = Cathegorie::findOrFail($id);

        return view('cathegories.show', [
            'cathegory' => $cathegory,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user();
        $this->authorize('testAdmin' , $user);

         $cathegory = Cathegory::findOrFail($id);
        return view('cathegories.edit', ['cathegory' => $cathegory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',

        ];

        $request->validate($rules);




        $cathegory = Cathegory::findOrFail($id);
        $cathegory->fill($request->all());
        $cathegory->save();


        return redirect('/cathegories/' . $cathegory->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user = \Auth::user();
        $this->authorize('testAdmin' , $user);
         Cathegorie::destroy($id);

        return back();
    }
}
