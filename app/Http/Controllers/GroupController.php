<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class GroupController extends Controller
{

    public function index(Request $request)
    {
         $this->authorize('index', Group::class);

        $group = $request->session()->get('group');
        if (! $group ) {$group = [];}
            return view('group.index', ['users' => $group]);


        //$request->session()->forget('group');
        //echo "<pre>";
       // var_dump(($request->session()->get('group')));
    }

     public function addUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $group = $request->session()->get('group');
        //buscar usuarios


         if ($group == null) {
             $group = array();
         }

         $position = -1;
         foreach ($group as $key => $item) {
             if ($item->id == $id) {
                $position = $key;

                break;
             }
         }

         if ($position == -1) {
            $user->cantidad = 1;
             $request->session()->push('group', $user);
         }
         return redirect('/groups');

    }

    public function flush(Request $request)
    {

       $request->session()->forget('group');


        return back();
    }

}
