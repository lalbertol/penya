<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public static function total()
    {
        $total = 0;
         $basket = Session::get('basket');

         if (isset($basket)){
            foreach ($basket as $key => $item) {

               $total += $item->cantidad * $item->price;

         }



        return $total . "€";
    }
}

}
