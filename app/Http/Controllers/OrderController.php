<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = \Auth::user();

        $this->authorize('testOrder' , $user);

       $orders = Order::paginate(10);
        return view('orders.index', ['orders' => $orders],[
            'user' => $user
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \Auth::user();
         $order = Order::with('products')->findOrFail($id);
         $total = 0;


        return view('orders.show', ['order' => $order],['total' => $total],[
            'user' => $user
        ]);
        //$role = Role::with('users')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $order = Order::findOrFail($id);


        // return $order->paid;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pagar(Request $request, $id)
    {


        $order = Order::findOrFail($id);

        $order->fill(array('paid' => 1));
        $order->save();


        return redirect('/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generate(Request $request){


    }

        public function pdfAll()
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
        **/
         $orders = Order::all();



        $pdf = PDF::loadView('orders.pdf', ['orders' => $orders]);

        return $pdf->download('listado.pdf');
    }


         public function pdfOne($id)
    {
          $user = \Auth::user();
         $order = Order::with('products')->findOrFail($id);
         $total = 0;



        $pdf = PDF::loadView('orders.pdfOrder', ['order' => $order],['total' => $total],[
            'user' => $user
        ]);


    $data = ['link' => 'http://styde.net'];

    \Mail::send('emails.notificacion', $data, function ($message) {

        $message->from('email@styde.net', 'Styde.Net');

        $message->to('user@example.com')->subject('Notificación');

    });




        return $pdf->download('pedido.pdf');
    }

}
