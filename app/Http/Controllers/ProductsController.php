<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class ProductsController extends Controller
{
       public function __construct()
  {
      $this->middleware('auth'); //aplicable a todos los métodos
      //$this->middleware('log')->only('index');//solamente a ....
      //$this->middleware('subscribed')->except('store');//todos excepto ...
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = \Auth::user();
        $products = Product::paginate(10);
        //return $products;
        return view('products.index', ['products' => $products] ,['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required',
            'cathegory_id' => 'required',

            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);


        // $user = new User();
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));

        // $user->remember_token = str_random(10);
        // $user->save();

        // //opcion2
        // $user = User::create($request->all());

        //opcion3
        $product = new Product();
        $product->fill($request->all());

        $product->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $product = Product::findOrFail($id);


        return view('products.show', [
            'product' => $product,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user();
        $this->authorize('testAdmin' , $user);

        $product = Product::findOrFail($id);
        return view('products.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required',
            'cathegory_id' => 'required',


        ];

        $request->validate($rules);




        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return redirect('/products/' . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \Auth::user();
        $this->authorize('testAdmin' , $user);
        Product::destroy($id);

        return back();
    }


    public function pdf($id)
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
        **/
        $product = Product::findOrFail($id);

        $pdf = PDF::loadView('pdf.product', compact('product'));

        return $pdf->download('listado.pdf');
    }
}
