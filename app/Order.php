<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
        protected $fillable = [
        'date','user_id','paid'
    ];

    public function products(){
        return $this->belongsToMany(\App\Product::class)->withPivot('quantity');
    }

   public function user()

    {

        return $this->belongsTo(\App\User::class);

    }


}
