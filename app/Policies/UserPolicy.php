<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update( User $model)
    {
         $user = \Auth::user();
        //dd($model);
        //echo($user->role_id);
        //echo($model->role_id);
        if ($user->role_id >= "3" )   {
            return true;
       }else{
            return false;
         }

        //return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $model)
    {
        $user = \Auth::user();
        if ($user->id != $model->id && $user->role_id >= "3") {
            return true;
        }



    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }

    public function index(User $user)
    {
        if ($user->role_id != "1") {
            return true;
        }
    }
      public function testAdmin(User $user)
    {
        if ($user->role_id >= "3"  ) {
            return true;
        }
    }

      public function testOrder(User $user)
    {
        if ($user->role_id > "1") {
            return true;
        }
    }
 public function testOrder2(User $user)
    {

        //$model = \Auth::user();
        //dd($model);
        if ($user == \Auth::user() )   {
            return true;
        }
    }


}
