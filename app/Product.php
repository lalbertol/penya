<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      protected $fillable = [
        'name','price','cathegory_id'
    ];

   public function cathegory(){
        return $this->belongsTo(\App\Cathegory::class);
        //hasMany / belongsTo / hasOne / belongsToMany
    }



}

