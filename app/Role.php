<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->hasMany(\App\User::class);
    }

    //  public function products(){
    //     return $this->hasMany(Product::class);
    // }
    // public function cathegories(){
    //     return $this->hasMany(Cathegorie::class);
    // }
}
