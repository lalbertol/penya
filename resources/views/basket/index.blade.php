@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Carrito</h1>


      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio unidad</th>
            <th>Categoria</th>
            <th>Cantidad</th>
            <th>Precio </th>

            <th>Operaciones</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($products as $product)
          <tr>
            <td>{{ $product->name }} </td>
             <td>{{ $product->price }} € </td>
              <td>{{ $product->cathegory->name }}  </td>
              <td>{{$product->cantidad}}</td>
              {{! $total += $product->price * $product->cantidad }}
              <td> {{$product->price * $product->cantidad }} €</td>

            <td>

              <form method="post" action="/basket/{{ $product->id }}">

                <a class="btn btn-primary"  role="button"
                href="/basket/add/{{ $product->id }}">
                +
              </a>

              <a class="btn btn-primary"  role="button"
                href="/basket/reduce/{{ $product->id }}">
                -
              </a>

              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">

              <input type="submit" value="borrar" class="btn btn-primary">
            </form>




          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay productos!!</td></tr>
        @endforelse
      </tbody>
    </table>


 <h3> Precio a pagar {{ $total }} €</h3>

<a href="/basket/flush"> Borrar </a>
<a href="/basket/store"> Guardar </a>
  </div>
</div>
</div>
@endsection
