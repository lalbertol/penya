@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
    <h1>Editar Categoria</h1>

    <form method="post" action="/cathegories/{{ $cathegory->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $cathegory->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
