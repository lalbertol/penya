<?php use App\User; ?>
@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Orders</h1>
     {{--  <div class="alert">
        <a href="/users/create" class="btn btn-primary">Nuevo</a>
      </div> --}}

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id Order</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Comprador</th>
            <th>Opciones</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($orders as $order)

          @can ('view',$order)
          <tr>
            <td>{{ $order->id }}</td>

            <td>{{ $order->date }}</td>
            <td>{{ $order->paid }}</td>
            <td>{{ $order->user->name }}</td>
            <td>

               <form method="post" action="/orders/{{ $order->id }}">
                <a class="btn btn-primary"  role="button"
                href="/orders/{{ $order->id }}">
                Ver
              </a>


            </form>
            </td>
        @endcan
        </tr>


        @empty
        <tr><td colspan="4">No hay orders!!</td></tr>
        @endforelse
      </tbody>
    </table>
    {{ $orders->render() }}

     <a href="{{ route('products.pdf') }}" class="btn btn-sm btn-primary">
            Descargar productos en PDF
        </a>
  </div>
</div>
</div>
@endsection
