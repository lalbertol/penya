<?php use App\User; ?>

<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Orders</h1>
     {{--  <div class="alert">
        <a href="/users/create" class="btn btn-primary">Nuevo</a>
      </div> --}}

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id Order</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Comprador</th>

          </tr>
        </thead>


        <tbody>


          @forelse ($orders as $order)

          @can ('view',$order)
          <tr>
            <td>{{ $order->id }}</td>

            <td>{{ \Carbon\Carbon::parse($order->date)->format('d-m-Y') }}</td>
            <td>{{ $order->paid }}</td>
            <td>{{ $order->user->name }}</td>
                  @endcan
        </tr>


        @empty
        <tr><td colspan="4">No hay orders!!</td></tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
</div>
