@extends('layouts.app')

@section('title', 'Categorias')

@section('content')
<style type="text/css">
    .alert {
      padding: 5px;
      background-color: #faa; /* Red */
      margin: 5px;
    }
</style>
    <h1>Alta de usuarios</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
    --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
    @endif

    <form method="post" action="/products">
        {{ csrf_field() }}

        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>
         <label>Precio</label>
        <input type="number" name="price" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>
        <br>
         <label>Id Categoria</label>
        <input type="text" name="cathegory_id" value="{{ old('cathegory_id') }}">
        <div class="alert alert-danger">
            {{ $errors->first('cathegory_id') }}
        </div>
        <br>

        <input type="submit" value="Nuevo">
    </form>
@endsection
