@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
    <h1>Editar Productos</h1>

    <form method="post" action="/products/{{ $product->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

      <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $product->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <label>Precio</label>
        <input type="price" name="price"
        value="{{ old('price') ? old('price') : $product->price }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>
        <br>
        <label>Id Categoria</label>
        <input type="text" name="cathegory_id"
        value="{{ old('cathegory_id') ? old('cathegory_id') : $product->cathegory_id }}">
        <div class="alert alert-danger">
            {{ $errors->first('cathegory_id') }}
        </div>
        <br>


        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
