@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de productos</h1>
      <div class="alert">
        <a href="/products/create" class="btn btn-primary">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Categoria</th>
            <th>Operaciones</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($products as $product)
          <tr>
            <td>{{ $product->name }} </td>
             <td>{{ $product->price }} € </td>
              <td>{{ $product->cathegory->name }}  </td>

            <td>

              <form method="post" action="/products/{{ $product->id }}">
                <a class="btn btn-primary"  role="button"
                href="/products/{{ $product->id }}">
                Ver
              </a>
              @can ('testAdmin', $user)
                <a class="btn btn-primary"  role="button"
                href="/products/{{ $product->id }}/edit">
                Editar
              </a>
              @endcan
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              @can ('testAdmin', $user)
              <input type="submit" value="borrar" class="btn btn-primary">
            </form>
            @endcan

              <a class="btn btn-success" href="/basket/{{ $product->id }}"> Añadir Carrito</a>

          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay productos!!</td></tr>
        @endforelse
      </tbody>
    </table>

    {{ $products->render() }}
  </div>
</div>
</div>
@endsection
