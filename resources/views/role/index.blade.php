@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de roles</h1>
     {{--  <div class="alert">
        <a href="/users/create" class="btn btn-primary">Nuevo</a>
      </div> --}}

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Id ROl</th>



          </tr>
        </thead>


        <tbody>


          @forelse ($roles as $role)
          <tr>
            <td>{{ $role->name }}</td>
            <td>{{ $role->id }}</td>


        </tr>
        @empty
        <tr><td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection
