@extends('layouts.app')

@section('title', 'Roles')

@section('content')

    <h1>
        Este es el detalle del usuario <?php echo $role->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $role->name }}</li>

    </ul>
    <h2>Lista de usuarios</h2>

    <ul>
    @foreach($role->users as $user)
    <li>{{$user->name}}</li>

    @endforeach
    </ul>

     <a class="btn btn-primary"  role="button"
                href="/users/">
                Volver
 </a>

@endsection

