@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

    <h1>
        Este es el detalle del usuario <?php echo $user->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $user->name }}</li>
        <li>Email: {{ $user->email }}</li>
    </ul>
 <a class="btn btn-primary"  role="button"
                href="/users/">
                Volver
 </a>
@endsection

