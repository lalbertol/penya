<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::get('users/{id}/edit2', 'UserController@edit2');
Route::resource('users', 'UserController');
Route::resource('cathegories', 'CathegoriesController');
Route::resource('products', 'ProductsController');
Route::resource('roles', 'RoleController');
Route::resource('group', 'GroupController');
//Route::resource('orders', 'OrderController');





////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/groups', 'GroupController@index');
Route::get('/groups/flush', 'GroupController@flush');
Route::get('/groups/{id}', 'GroupController@addUser');


// Route::resource('basket', 'BasketController');
Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/store', 'BasketController@store');
Route::get('/basket/{id}', 'BasketController@addSession');
Route::get('/basket/add/{id}', 'BasketController@add');
Route::get('/basket/reduce/{id}', 'BasketController@reduce');

Route::delete('basket/{id}', 'BasketController@destroy');


//Route::get('orders/pdf', 'OrderController@generate');
Route::get('generate-pdf', 'OrderController@generate')->name('generate-pdf');
Route::get('/orders', 'OrderController@index');

Route::get('orders/{id}/pagar', 'OrderController@pagar');
Route::get('/orders/{id}', 'OrderController@show');


Route::get ( '/redirect/{service}', 'Auth\LoginController@redirect' );

Route::get ( '/callback/{service}', 'Auth\LoginController@callback' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('descargar-productos', 'OrderController@pdfAll')->name('products.pdf');
Route::get('descargar-producto/{id}', 'OrderController@pdfOne')->name('product.pdf');
    Route::post('/enviarCorreo', 'EmailController@enviarCorreo');

/*Route::get('/test/', function () {
  $pdf = PDF::loadView('pruebaparapdf');
  return $pdf->download('pruebapdf.pdf');
});
*/
